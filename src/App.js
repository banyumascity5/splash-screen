import React from 'react'
import {StyleSheet} from 'react-native'
import {NavigationContainer} from '@react-navigation/native'
import Reute from './Route/index'

const App = () => {
  return (
    <NavigationContainer>
      <Reute />
    </NavigationContainer>
  )
}

export default App

const style = StyleSheet. create({});