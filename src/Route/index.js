import React from 'react'
import {createStackNavigator} from '@react-navigation/stack'
import Splash from '../Pages/Splash'
import Youtube from '../Pages/Youtube';

const Stack = createStackNavigator();

const Reute = () => {
    return (
    <Stack.Navigator
        initialRouteName="Splash"
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="Splash" component={Splash} />
        <Stack.Screen name="Youtube" component={Youtube}  />
    </Stack.Navigator>
    )
}

export default Reute