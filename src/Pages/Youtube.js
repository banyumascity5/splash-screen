import React from 'react'
import { View, Text, StyleSheet,TouchableOpacity,Image, Button, ActivityIndicator, ActivityIndicatorComponent } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';

const Youtube = ({navigation}) => {
    return (
      <View style={Setting.flex}>
        <View style={Setting.navBar}>
       <Image source= {require('../Assets/kk.png')} style={{width: 130, height: 40}} />
      <View style={Setting.righNav}>
         <TouchableOpacity>
           <Icon style={Setting.NavItem} name="search" size={30} />
         </TouchableOpacity>
         <TouchableOpacity>
         <Icon style={Setting.NavItem} name="account-circle" size={30} />
         </TouchableOpacity>
       </View>
       </View > 
       <View style={Setting.garis}>
       <View style={Setting.wrapper}>
         <Image source={require('../Assets/lj.png')}/>
       <Text style={{fontSize:20,fontWeight:'bold',color:'black',marginTop:10}}>Anda sedang offline</Text>
       <Text style={{fontSize:15,color:'gray'}}>Tonton video yang didonwload,</Text>
       <Text style={{fontSize:15,color:'gray'}}>atau temukan video baru untuk</Text>
       <Text style={{fontSize:15,color:'gray'}}>didownload di koleksi anda.</Text>
       <View style={Setting.Button}>
       <Button title="BUKA SEKARANG" color='blue' onPress={() => navigation.navigate('Liblery')}/>
       </View>
       <View>
       <ActivityIndicator size="large" color="blue" style={{margin:20}}/>
       </View>
       </View> 
       <View>
           <Image source={require('../Assets/bw.jpg')} style={{height:60,width:360}}/>
       </View>
       </View>
       </View>
       
    )
}

export default Youtube;

const Setting = StyleSheet. create({
  flex:{
    flex:2,
  },
    righNav: {
        marginVertical:5,
        flexDirection: 'row'
        
      },
      NavItem: {
        marginLeft: 20
      },
      navBar: {
        height: 45,
        backgroundColor: 'white',
        paddingHorizontal: 10,
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'space-between',
        elevation:6
        
      },
      garis: {
        flex:1
      },
      wrapper: {
        backgroundColor:'white',
        height:630,
        justifyContent:'center',
        alignItems:'center',
    },
    Button: {
      marginTop:20
    }
});
